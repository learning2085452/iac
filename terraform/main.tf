# resource "aws_vpc" "demovpc" {
#   cidr_block       = "${var.vpc_cidr}"
#   instance_tenancy = "default"

#   tags = {
#     Name = "Demo-VPC"
#   }
# }

# resource "aws_subnet" "demosubnet" {
#   vpc_id                  = aws_vpc.demovpc.id
#   cidr_block              = "${var.subnet_cidr}"
#   availability_zone       = "${var.aws_region}a"

#   tags = {
#     Name = "Demo-Subnet"
#   }
# }

resource "aws_vpc" "demovpc" {
  cidr_block       = var.vpc_cidr
  instance_tenancy = "default"

  tags = {
    Name = "Demo-VPC"
  }
}

# resource "aws_subnet" "demosubnet" {
#   vpc_id                  = aws_vpc.demovpc.id
#   cidr_block              = var.subnet_cidr
#   availability_zone       = "ap-southeast-1a"  # Adjust the availability zone as needed

#   tags = {
#     Name = "Demo-Subnet"
#   }
# }

# resource "aws_instance" "demoec2" {
#   ami           = "ami-0e4b5d31e60aa0acd"  # Specify the AMI ID of the desired Amazon Machine Image
#   instance_type = "t2.micro"      # Specify the instance type

#   tags = {
#     Name = "Demo-EC2"
#   }
# }